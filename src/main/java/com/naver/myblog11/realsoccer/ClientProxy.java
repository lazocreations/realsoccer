package com.naver.myblog11.realsoccer;

import com.naver.myblog11.realsoccer.client.events.ClientEvents;
import com.naver.myblog11.realsoccer.client.render.BallRender;
import com.naver.myblog11.realsoccer.common.entities.BallEntity;
import com.naver.myblog11.realsoccer.packet.KickMessage;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.client.registry.IRenderFactory;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ClientProxy extends CommonProxy {
    
    @Override
    public void preInit(FMLPreInitializationEvent event) {

        super.preInit(event);

        registerPackets();
        registerBallRender();
        registerItemRender(RealSoccer.ball, 0);

    }
    
    @Override
    public void init(FMLInitializationEvent event) {

        super.init(event);
        MinecraftForge.EVENT_BUS.register(new ClientEvents());
        
    }

    private void registerItemRender(Item item, int meta) {
        
        ResourceLocation loc = item.getRegistryName();
        
        if(loc == null) { return; }
        
        ModelResourceLocation modelLoc = new ModelResourceLocation(loc, "inventory");
        ModelLoader.setCustomModelResourceLocation(item, meta, modelLoc);
        
    }

    private void registerBallRender() {

        RenderingRegistry.registerEntityRenderingHandler(BallEntity.class, new IRenderFactory<BallEntity>() {

            @Override
            public Render<BallEntity> createRenderFor(RenderManager manager) {
                return new BallRender(manager);
            }

        });
        
    }

    private void registerPackets() {

        SimpleNetworkWrapper wrapper = INSTANCE;
        wrapper.registerMessage(KickMessage.handler, KickMessage.class, KickMessage.ID, Side.CLIENT);

    }
    
}