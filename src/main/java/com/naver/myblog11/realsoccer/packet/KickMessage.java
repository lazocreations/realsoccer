package com.naver.myblog11.realsoccer.packet;

import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class KickMessage implements IMessage {

    public static KickMessageHandler handler = new KickMessageHandler();
    public static final int ID = 0;

    // Default constructor is required for IMessage classes.
    public KickMessage() {}
    
    @Override
    public void fromBytes(ByteBuf buf) {
        
    }
    
    @Override
    public void toBytes(ByteBuf buf) {
        
    }
    
}

class KickMessageHandler implements IMessageHandler<KickMessage, IMessage> {

    @Override
    public IMessage onMessage(KickMessage message, MessageContext ctx) {
        return null;
    }

}