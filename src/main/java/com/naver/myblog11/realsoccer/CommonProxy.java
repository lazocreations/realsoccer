package com.naver.myblog11.realsoccer;

import com.naver.myblog11.realsoccer.common.entities.BallEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.common.registry.EntityRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class CommonProxy {

    public static final SimpleNetworkWrapper INSTANCE = NetworkRegistry.INSTANCE.newSimpleChannel("realsoccer");

    public void preInit(FMLPreInitializationEvent event) {
    
        GameRegistry.register(RealSoccer.ball);
        EntityRegistry.registerModEntity(new ResourceLocation(RealSoccer.MOD_ID, "EntityBall"), BallEntity.class,
                "EntityBall", 0, RealSoccer.getInstance(), 64, 3, true);
        
    }
    
    public void init(FMLInitializationEvent event) {}
    public void postInit(FMLPostInitializationEvent event) {}

}