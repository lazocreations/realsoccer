package com.naver.myblog11.realsoccer.client.events;

import com.naver.myblog11.realsoccer.RealSoccer;
import com.naver.myblog11.realsoccer.common.entities.BallEntity;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.Side;

public class ClientEvents {
    
    private final KeyBinding leftMouse = Minecraft.getMinecraft().gameSettings.keyBindAttack;
    private final KeyBinding rightMouse = Minecraft.getMinecraft().gameSettings.keyBindUseItem;

    @SubscribeEvent
    public void onTick(TickEvent.PlayerTickEvent event) {
        
        if(event.side.equals(Side.CLIENT) && event.phase.equals(TickEvent.Phase.START)) {
            
            //noinspection MethodCallSideOnly
            if(leftMouse.isKeyDown()) {
    
                EntityPlayer player = event.player;
                double posX = player.posX;
                double posY = player.posY;
                double posZ = player.posZ;
    
                AxisAlignedBB area = new AxisAlignedBB(posX - 1D, posY - 1D, posZ - 1D, posX + 1D, posY + 1D, posZ + 1D);
                double dist_ = Double.MAX_VALUE;
                BallEntity ball = null;
                
                for(BallEntity e : player.world.getEntitiesWithinAABB(BallEntity.class, area)) {
                    
                    double dist = e.getDistanceSqToEntity(player);
                    
                    if(dist < dist_) {
                        dist_ = dist;
                        ball = e;
                    }
                    
                }
                
                if(ball != null) {
                    
                    
                    
                }
                
                // this part is printed twice every click.
                RealSoccer.logger.info("Searching ball from centre: " + (float) posX + ", " + (float) posY + ", " + (float) posZ);
                
            }
            
        }
        
    }
    
}