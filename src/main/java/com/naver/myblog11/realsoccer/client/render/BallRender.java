package com.naver.myblog11.realsoccer.client.render;

import com.naver.myblog11.realsoccer.RealSoccer;
import com.naver.myblog11.realsoccer.common.entities.BallEntity;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;
import org.lwjgl.util.glu.Sphere;

public class BallRender extends Render<BallEntity> {
    
    private final ResourceLocation rl = new ResourceLocation(RealSoccer.MOD_ID, "textures/balls/jabulani.jpg");
    private int sphereOutside;
    
    public BallRender(RenderManager renderManager) {
        super(renderManager);
    }
    
    @Override
    protected ResourceLocation getEntityTexture(BallEntity entity) {
        return rl;
    }
    
    private void drawSphere() { // TODO put it on init()
        
        Sphere sphere = new Sphere();
        sphere.setDrawStyle(GLU.GLU_FILL);
        sphere.setNormals(GLU.GLU_SMOOTH);
        sphere.setTextureFlag(true);
        sphere.setOrientation(GLU.GLU_OUTSIDE);

        sphereOutside = GL11.glGenLists(1);
        GL11.glNewList(sphereOutside, GL11.GL_COMPILE);
        bindTexture(rl);
        sphere.draw(0.5F, 32, 32);
        GL11.glEndList();
        
    }
    
    @Override
    public void doRender(BallEntity entity, double x, double y, double z, float entityYaw, float partialTicks) {
    
        GL11.glPushMatrix();
        GL11.glTranslated(x, y + 0.5, z);
        GL11.glScalef(1.0F, 1.0F, 1.0F);
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 0.95F);
        GL11.glEnable(GL11.GL_ALPHA_TEST);
        drawSphere();
        GL11.glCallList(sphereOutside);
        GL11.glPopMatrix();
        
    }

}