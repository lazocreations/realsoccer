package com.naver.myblog11.realsoccer;

import com.naver.myblog11.realsoccer.common.items.Ball;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.FMLLog;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import org.apache.logging.log4j.Logger;

@Mod(
        modid = RealSoccer.MOD_ID,
        name = RealSoccer.MOD_NAME,
        version = RealSoccer.VERSION
)
public class RealSoccer {
    
    public static final String MOD_ID = "realsoccer";
    public static final String MOD_NAME = "RealSoccer";
    public static final String VERSION = "1.0-SNAPSHOT";
    public static Logger logger;
    public static final Item ball = new Ball();
    
    @Mod.Instance(value = MOD_ID)
    public static RealSoccer instance;
    
    @SidedProxy(modId = MOD_ID, clientSide = "com.naver.myblog11.realsoccer.ClientProxy",
            serverSide = "com.naver.myblog11.realsoccer.CommonProxy")
    public static CommonProxy proxy;
    
    
    public static RealSoccer getInstance() {
        return instance;
    }

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        logger = FMLLog.getLogger();
        proxy.preInit(event);
    }
    
    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        proxy.init(event);
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        proxy.postInit(event);
    }
    
}