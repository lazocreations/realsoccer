package com.naver.myblog11.realsoccer.common.items;

import com.naver.myblog11.realsoccer.common.entities.BallEntity;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class Ball extends Item {
    
    public Ball() {
        
        setCreativeTab(CreativeTabs.MISC);
        setMaxStackSize(1);
        setNoRepair();
        setUnlocalizedName("nike_ordem_4");
        setRegistryName("nike_ordem_4");
        
    }
    
    @Override
    public EnumActionResult onItemUse(EntityPlayer player, World world, BlockPos pos, EnumHand hand,
                                EnumFacing facing, float hitX, float hitY, float hitZ) {
    
        ItemStack item = player.getHeldItem(hand);
        BallEntity entity = new BallEntity(world);
            
        int x = pos.getX();
        int y = pos.getY() + 1;
        int z = pos.getZ();
        
        if(world.isAirBlock(new BlockPos(x, y, z))) {
            
            if(!world.isRemote) {
                entity.setPositionAndRotation(x + 0.5D, y, z + 0.5D, 0.0F, 0.0F);
                world.spawnEntity(entity);
            }
            
            if(!player.capabilities.isCreativeMode) {
                item.shrink(1);
            }
            
            return EnumActionResult.SUCCESS;
            
        }
        
        return EnumActionResult.FAIL;
        
    }
    
}