package com.naver.myblog11.realsoccer.common.entities;

import com.naver.myblog11.realsoccer.RealSoccer;
import net.minecraft.entity.MoverType;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;

public class BallPhysic {
    
    private BallEntity ball;
    private double prevPosX;
    private double prevPosY;
    private double prevPosZ;
    
    public BallPhysic(BallEntity ball) {
        this.ball = ball;
    }
    
    public boolean isHeader(EntityPlayer player, double radius) {
        return ball.getPositionVector().distanceTo(player.getPositionEyes(1.0F)) < radius;
    }
    
    public boolean isFooter(EntityPlayer player, double radius) {
        return ball.getPositionVector().distanceTo(player.getPositionVector()) < radius;
    }
    
    public void putOnGround() {
        
        BlockPos pos = ball.getEntityWorld().getTopSolidOrLiquidBlock(ball.getPosition());
        ball.setPosition(ball.posX, pos.getY(), ball.posZ);
        
    }
    
    public boolean isOnGround() {
        int gnd = ball.getEntityWorld().getTopSolidOrLiquidBlock(ball.getPosition()).getY();
        return (gnd == ball.posY);
    }
    
    /**
     * If the ball's velocity > 0, will move it into the next frame (motionX/Y/Z).
     * Gravitation and air friction will be applied.
     */
    public void applyMovement() {
        
        if(isOnGround() && ball.get3DVelocity() == 0F) {
            RealSoccer.logger.info("Ball is not moving, sticking on ground.");
            return;
        }
    
        prevPosX = ball.posX;
        prevPosY = ball.posY;
        prevPosZ = ball.posZ;
        
        applyGravity();
        applyAirFriction();
    
        ball.move(MoverType.SELF, ball.motionX, ball.motionY, ball.motionZ);
        applyBounce(0.2F);
        
    }
    
    /**
     * Should be called after moving ball! i.e. ball.move()
     * TODO adjust absorption according to height.
     * @param absorb Potential energy absorption percentages. Valid gap is between 0.0 and 1.0
     */
    private void applyBounce(float absorb) {
        
        if(ball.isCollidedVertically) {
    
            double frame = ball.posY - prevPosY;
            
            if(Math.abs(frame) > 0.05F) {
            
                // Make ball bounce up or down
                ball.motionY = frame * -(1F - absorb);
                
                RealSoccer.logger.info("BOUNCE! MotionY: " + ball.motionY);
            
            }
            
            if(ball.get3DVelocity() > 0) {
                ball.move(MoverType.SELF, ball.motionX, ball.motionY, ball.motionZ);
            }
        
        }
        
    }
    
    private void applyGravity() {
        
        if(!isOnGround()) {
            ball.motionY -= 0.04D;
        }
        
    }
    
    private void applyAirFriction() {
        
        double velocity = ball.get2DVelocity();
        double decelerate = 0.95D;
        
        if(velocity < 0.01F && isOnGround()) {
            decelerate = 0D;
        } else if(velocity < 0.1F) {
            decelerate = 0.85D;
        } else if(velocity < 0.2F) {
            decelerate = 0.9D;
        }
        
        if(ball.motionY > 0D) { ball.motionY *= 0.99D; }
        ball.motionX *= decelerate;
        ball.motionZ *= decelerate;
        
    }
    
}