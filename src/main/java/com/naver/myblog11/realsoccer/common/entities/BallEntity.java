package com.naver.myblog11.realsoccer.common.entities;

import com.naver.myblog11.realsoccer.RealSoccer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;

import javax.vecmath.Vector2d;
import java.text.DecimalFormat;

public class BallEntity extends Entity {
    
    private static final DataParameter<Float> DAMAGE = EntityDataManager.createKey(BallEntity.class, DataSerializers.FLOAT);

    private BallPhysic physic;
    
    public BallEntity(World world) {
        super(world);
        setSize(1.25F, 1.0F);
        setInvisible(false);
        setGlowing(false);
        setNoGravity(false);

        physic = new BallPhysic(this);
    }
    
    @Override
    public void entityInit() {

        this.dataManager.register(DAMAGE, 0.0F);
        
    }
    
    @Override
    public void onCollideWithPlayer(EntityPlayer player) {

        if(this.world.isRemote || player.noClip || this.noClip) {
           return;
        }
        
        double x = this.posX - player.posX;
        double y = this.posY - player.posY;
        double z = this.posZ - player.posZ;
        Vec3d vec = new Vec3d(x, y, z);
        Vec3d faceVec = player.getLookVec();
        boolean keyPressed = false;
        
        
        if(physic.isHeader(player, 0.8D)) {
            
            if(keyPressed) {
    
                player.sendStatusMessage(new TextComponentString("HEADER"), true);
                
            }
            else {
                
                
                
            }
            
        }
        else if(physic.isFooter(player, 0.7D)) {
            
            if(keyPressed) {
                
                player.sendStatusMessage(new TextComponentString("Kicked"), true);
                
            }
            else {
    
                Vec3d newVec = vec;
                double scala = 0.45D;
    
                if(faceVec.yCoord > 0D) {
                    faceVec = new Vec3d(faceVec.xCoord, 0D, faceVec.zCoord);
                }
                if(player.isSneaking()) {
                    // Foot direction only affects ball vector
                    scala = 0.25D;
                }
                else {
                    // Facing and foot direction equivalently affects ball vector
                    newVec = vec.add(faceVec);
        
                    if(player.isSprinting()) {
                        // Facing direction greatly affects ball vector
                        scala = 0.7D;
                        newVec = vec.add(faceVec);
                    }
                }
    
                newVec = newVec.scale(scala * (vec.lengthVector() / newVec.lengthVector()));
                this.addVelocity(newVec.xCoord, newVec.yCoord, newVec.zCoord);
                
            }
            
        }
        
    }
    
    @Override
    public void onUpdate() {
        
        super.onUpdate();
    
        if(this.isDead) { return; }
    
        if(getDamage() > 0.0F) {
            addDamage(-1.0F);
        }
        
        this.onGround = physic.isOnGround();
        physic.applyMovement();
        
    }
    
    @Override
    public boolean hitByEntity(Entity entity) {
        
        if(entity instanceof EntityPlayer && get3DVelocity() == 0F) {
            attackEntityFrom(DamageSource.causePlayerDamage((EntityPlayer) entity), 5.0F);
            return false;
        }
        
        return false;
    }
    
    @Override
    public boolean attackEntityFrom(DamageSource source, float amount) {
        
        if(!this.isDead) {
    
            if (this.isEntityInvulnerable(source)) {
                return false;
            }
            
            this.addDamage(amount);
            this.setBeenAttacked();
            boolean creative = source.isCreativePlayer();
            
            if(creative || this.getDamage() >= 20.0F) {
                
                this.setDead();
                
                if(!this.world.isRemote && !creative && this.world.getGameRules().getBoolean("doEntityDrops")) {
                    
                    ItemStack item = new ItemStack(RealSoccer.ball);
                    this.entityDropItem(item, 0.5F);
                    
                }
                
            }
    
        }

        return true;

    }
    
    public float get2DVelocity() {
        Vector2d vec = new Vector2d(this.motionX, this.motionZ);
        return Float.valueOf(new DecimalFormat("#.###").format(vec.length()));
    }
    
    public float get3DVelocity() {
        Vec3d vec = new Vec3d(this.motionX, this.motionY, this.motionZ);
        return Float.valueOf(new DecimalFormat("#.###").format(vec.lengthVector()));
    }
    
    public void addDamage(float damage) {
        float value = dataManager.get(DAMAGE);
        dataManager.set(DAMAGE, value + damage);
    }
    
    public float getDamage() {
        return dataManager.get(DAMAGE);
    }
    
    public BallPhysic getPhysic() { return physic; }
    
    @Override
    public boolean canBeCollidedWith() {
        return !this.isDead;
    }

    @Override
    public boolean canBePushed() {
        return false;
    }

    @Override
    public boolean canTriggerWalking() {
        return false;
    }
    
    @Override
    public AxisAlignedBB getCollisionBox(Entity entity) {
        return entity.canBePushed() ? entity.getEntityBoundingBox() : null;
    }

    @Override
    protected void readEntityFromNBT(NBTTagCompound compound) {
        
    }
    
    @Override
    protected void writeEntityToNBT(NBTTagCompound compound) {
        
    }
    
}